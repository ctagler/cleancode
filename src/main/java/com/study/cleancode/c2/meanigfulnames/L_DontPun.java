package com.study.cleancode.c2.meanigfulnames;

/**
 *
 * @author ctagler
 */
public class L_DontPun {
    /**
     * Avoid using the same word for two purposes. Using the same term for two
     * different ideas is essentially a pun.
     *
     * If you follow the “one word per concept” rule, you could end up with many
     * classes that have, for example, an add method. As long as the parameter
     * lists and return values of the various add methods are semantically
     * equivalent, all is well.
     *
     * However one might decide to use the word add for “consistency” when he or
     * she is not in fact adding in the same sense. Let’s say we have many
     * classes where add will create a new value by adding or concatenating two
     * existing values. Now let’s say we are writing a new class that has a
     * method that puts its single parameter into a collection. Should we call
     * this method add? It might seem consistent because we have so many other
     * add methods, but in this case, the semantics are different, so we should
     * use a name like insert or append instead. To call the new method add
     * would be a pun.
     *
     * Our goal, as authors, is to make our code as easy as possible to
     * understand. We want our code to be a quick skim, not an intense study. We
     * want to use the popular paperback model whereby the author is responsible
     * for making himself clear and not the academic model where it is the
     * scholar’s job to dig the meaning out of the paper.
     */
}
