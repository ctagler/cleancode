package com.study.cleancode.c2.meanigfulnames;

import com.study.cleancode.c2.meanigfulnames.classes.PhoneNumber;

/**
 *
 * @author ctagler
 */
public class F_AvoidEncodings {

    public F_AvoidEncodings() {
        /**
         * We have enough encodings to deal with without adding more to our
         * burden. Encoding type or scope information into names simply adds an
         * extra burden of deciphering. It hardly seems reasonable to require
         * each new employee to learn yet another encoding “language” in
         * addition to learning the (usually considerable) body of code that
         * they’ll be working in. It is an unnecessary mental burden when trying
         * to solve a problem. Encoded names are seldom pronounceable and are
         * easy to mistype.
         */
    }

    public void HungarianNotation() {
        /**
         * In days of old, when we worked in name-length-challenged languages,
         * we violated this rule out of necessity, and with regret. Fortran
         * forced encodings by making the first letter a code for the type.
         * Early versions of BASIC allowed only a letter plus one digit.
         * Hungarian Notation (HN) took this to a whole new level.
         *
         * HN was considered to be pretty important back in the Windows C API,
         * when everything was an integer handle or a long pointer or a void
         * pointer, or one of several implementations of “string” (with
         * different uses and attributes). The compiler did not check types in
         * those days, so the programmers needed a crutch to help them remember
         * the types.
         *
         * In modern languages we have much richer type systems, and the
         * compilers remember and enforce the types. What’s more, there is a
         * trend toward smaller classes and shorter functions so that people can
         * usually see the point of declaration of each variable they’re using.
         *
         * Java programmers don’t need type encoding. Objects are strongly
         * typed, and editing environments have advanced such that they detect a
         * type error long before you can run a compile! So nowadays HN and
         * other forms of type encoding are simply impediments. They make it
         * harder to change the name or type of a variable, function, or class.
         * They make it harder to read the code. And they create the possibility
         * that the encoding system will mislead the reader.
         */

        PhoneNumber phoneString; // name not changed when type changed!
    }

    public void MemberPrefixes() {
        /**
         * You also don’t need to prefix member variables with m_ anymore. Your
         * classes and functions should be small enough that you don’t need
         * them. And you should be using an editing environment that highlights
         * or colorizes members to make them distinct.
         */
    }

    public class Part_Wrong {

        private String m_dsc; // The textual description

        void setName(String name) {
            m_dsc = name;
        }
    }

    public class Part_Good {

        String description;

        void setDescription(String description) {
            this.description = description;
        }
    }

    /**
     * Besides, people quickly learn to ignore the prefix (or suffix) to see the
     * meaningful part of the name. The more we read the code, the less we see
     * the prefixes. Eventually the prefixes become unseen clutter and a marker
     * of older code.
     */
    public void InterfacesAndImplementations() {
        /**
         * These are sometimes a special case for encodings. For example, say
         * you are building an ABSTRACT FACTORY for the creation of shapes. This
         * factory will be an interface and will be implemented by a concrete
         * class. What should you name them? IShapeFactory and ShapeFactory ? I
         * prefer to leave interfaces unadorned. The preceding I, so common in
         * today’s legacy wads, is a distraction at best and too much
         * information at worst. I don’t want my users knowing that I’m handing
         * them an interface. I just want them to know that it’s a ShapeFactory.
         * So if I must encode either the interface or the implementation, I
         * choose the implementation. Calling it ShapeFactoryImp, or even the
         * hideous CShapeFactory, is preferable to encoding the interface.
         */
    }
}
