package com.study.cleancode.c2.meanigfulnames;

/**
 *
 * @author ctagler
 */
public class B_AvoidDisinformation {

    /**
     * Programmers must avoid leaving false clues that obscure the meaning of
     * code. We should avoid words whose entrenched meanings vary from our
     * intended meaning. For example, hp, aix, and sco would be poor variable
     * names because they are the names of Unix platforms or variants. Even if
     * you are coding a hypotenuse and hp looks like a good abbreviation, it
     * could be disinformative.
     *
     * Do not refer to a grouping of accounts as an accountList unless it’s
     * actually a List. The word list means something specific to programmers.
     * If the container holding the accounts is not actually a List, it may lead
     * to false conclusions. So accountGroup or bunchOfAccounts or just plain
     * accounts would be better.
     *
     * Beware of using names which vary in small ways. How long does it take to
     * spot the subtle difference between a
     * XYZControllerForEfficientHandlingOfStrings in one module and, somewhere a
     * little more distant, XYZControllerForEfficientStorageOfStrings? The words
     * have frightfully similar shapes.
     *
     * Spelling similar concepts similarly is information. Using inconsistent
     * spellings is dis- information. With modern Java environments we enjoy
     * automatic code completion. We write a few characters of a name and press
     * some hotkey combination (if that) and are rewarded with a list of
     * possible completions for that name. It is very helpful if names for very
     * similar things sort together alphabetically and if the differences are
     * very obvious, because the developer is likely to pick an object by name
     * without seeing your copious comments or even the list of methods supplied
     * by that class.
     *
     * A truly awful example of disinformative names would be the use of
     * lower-case L or uppercase O as variable names, especially in combination.
     * The problem, of course, is that they look almost entirely like the
     * constants one and zero, respectively.
     */
    int l, O, O1;

    public void someExample() {
        int a = l;
        if (O == l) {
            a = O1;
        } else {
            l = 01;
        }
    }

    /**
     * The reader may think this a contrivance, but we have examined code where
     * such things were abundant. In one case the author of the code suggested
     * using a different font so that the differences were more obvious, a
     * solution that would have to be passed down to all future developers as
     * oral tradition or in a written document. The problem is conquered with
     * finality and without creating new work products by a simple renaming.
     */
}
