package com.study.cleancode.c2.meanigfulnames;

import com.study.cleancode.c2.meanigfulnames.classes.Cell;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ctagler
 */
public class A_UseIntentionRevealingNames {

    /**
     * It is easy to say that names should reveal intent. What we want to
     * impress upon you is that we are serious about this. Choosing good names
     * takes time but saves more than it takes. So take care with your names and
     * change them when you find better ones. Everyone who reads your code
     * (including you) will be happier if you do.
     *
     * The name of a variable, function, or class, should answer all the big
     * questions. It should tell you why it exists, what it does, and how it is
     * used. If a name requires a comment, then the name does not reveal its
     * intent.
     */
    int d; // elapsed time in days

    /**
     * The name d reveals nothing. It does not evoke a sense of elapsed time,
     * nor of days. We should choose a name that specifies what is being
     * measured and the unit of that measurement:
     */
    int elapsedTimeInDays;
    int daysSinceCreation;
    int daysSinceModification;
    int fileAgeInDays;

    /**
     * Choosing names that reveal intent can make it much easier to understand
     * and change code. What is the purpose of this code?
     */
    List<int[]> theList;

    public List<int[]> getThem() {
        List<int[]> list1 = new ArrayList<>();
        for (int[] x : theList) {
            if (x[0] == 4) {
                list1.add(x);
            }
        }
        return list1;
    }

    /**
     * Why is it hard to tell what this code is doing? There are no complex
     * expressions. Spacing and indentation are reasonable. There are only three
     * variables and two constants mentioned. There aren’t even any fancy
     * classes or polymorphic methods, just a list of arrays (or so it seems).
     *
     * The problem isn’t the simplicity of the code but the implicity of the
     * code (to coin a phrase): the degree to which the context is not explicit
     * in the code itself. The code implicitly requires that we know the answers
     * to questions such as:
     *
     * 1. What kinds of things are in theList?
     *
     * 2. What is the significance of the zeroth subscript of an item in
     * theList?
     *
     * 3. What is the significance of the value 4?
     *
     * 4. How would I use the list being returned?
     *
     * The answers to these questions are not present in the code sample, but
     * they could have been. Say that we’re working in a mine sweeper game. We
     * find that the board is a list of cells called theList . Let’s rename that
     * to gameBoard.
     *
     * Each cell on the board is represented by a simple array. We further find
     * that the zeroth subscript is the location of a status value and that a
     * status value of 4 means “flagged.” Just by giving these concepts names we
     * can improve the code considerably:
     */
    int FLAGGED = 4;
    int STATUS_VALUE = 0;
    List<int[]> gameBoard;

    public List<int[]> getFlaggedCells() {
        List<int[]> flaggedCells = new ArrayList<>();
        for (int[] cell : gameBoard) {
            if (cell[STATUS_VALUE] == FLAGGED) {
                flaggedCells.add(cell);
            }
        }
        return flaggedCells;
    }

    /**
     * Notice that the simplicity of the code has not changed. It still has
     * exactly the same number of operators and constants, with exactly the same
     * number of nesting levels. But the code has become much more explicit.
     *
     * We can go further and write a simple class for cells instead of using an
     * array of ints. It can include an intention-revealing function (call it
     * isFlagged ) to hide the magic numbers. It results in a new version of the
     * function:
     */
    List<Cell> gameBoardNew;

    public List<Cell> getFlaggedCellsNew() {
        List<Cell> flaggedCells = new ArrayList<>();
        for (Cell cell : gameBoardNew) {
            if (cell.isFlagged()) {
                flaggedCells.add(cell);
            }
        }
        return flaggedCells;
    }

    /**
     * With these simple name changes, it’s not difficult to understand what’s
     * going on. This is the power of choosing good names.
     */
}
