package com.study.cleancode.c2.meanigfulnames;

import java.util.Date;

/**
 *
 * @author ctagler
 */
public class D_UsePronounceableNames {

    /**
     * Humans are good at words. A significant part of our brains is dedicated
     * to the concept of words. And words are, by definition, pronounceable. It
     * would be a shame not to take advantage of that huge portion of our brains
     * that has evolved to deal with spoken language. So make your names
     * pronounceable.
     *
     * If you can’t pronounce it, you can’t discuss it without sounding like an
     * idiot. “Well, over here on the bee cee arr three cee enn tee we have a
     * pee ess zee kyew int, see?” This matters because programming is a social
     * activity.
     *
     * A company I know has genymdhms (generation date, year, month, day, hour,
     * minute, and second) so they walked around saying “gen why emm dee aich
     * emm ess”. I have an annoying habit of pronouncing everything as written,
     * so I started saying “gen-yah-mudda-hims.” It later was being called this
     * by a host of designers and analysts, and we still sounded silly. But we
     * were in on the joke, so it was fun. Fun or not, we were tolerating poor
     * naming. New developers had to have the variables explained to them, and
     * then they spoke about it in silly made-up words instead of using proper
     * English terms. Compare
     */
    class DtaRcrd102 {

        private Date genymdhms;
        private Date modymdhms;
        private final String pszqint = "102";
        /* ... */
    };

    /**
     * to
     */
    class Customer {

        private Date generationTimestamp;
        private Date modificationTimestamp;

        private final String recordId = "102";
        /* ... */
    }

    /**
     * Intelligent conversation is now possible: “Hey, Mikey, take a look at
     * this record! The generation timestamp is set to tomorrow’s date! How can
     * that be?”
     */
}
