package com.study.cleancode.c2.meanigfulnames;

/**
 *
 * @author ctagler
 */
public class P_DontAddGratuitousContext {
    /**
     * In an imaginary application called “Gas Station Deluxe,” it is a bad idea
     * to prefix every class with GSD. Frankly, you are working against your
     * tools. You type G and press the completion key and are rewarded with a
     * mile-long list of every class in the system. Is that wise? Why make it
     * hard for the IDE to help you?
     *
     * Likewise, say you invented a MailingAddress class in GSD’s accounting
     * module, and you named it GSDAccountAddress. Later, you need a mailing
     * address for your customer contact application. Do you use
     * GSDAccountAddress? Does it sound like the right name? Ten of 17
     * characters are redundant or irrelevant.
     *
     * Shorter names are generally better than longer ones, so long as they are
     * clear. Add no more context to a name than is necessary.
     *
     * The names accountAddress and customerAddress are fine names for instances
     * of the class Address but could be poor names for classes. Address is a
     * fine name for a class. If I need to differentiate between MAC addresses,
     * port addresses, and Web addresses, I might consider PostalAddress, MAC,
     * and URI. The resulting names are more precise, which is the point of all
     * naming.
     */
}
