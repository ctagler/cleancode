package com.study.cleancode.c2.meanigfulnames;

/**
 *
 * @author ctagler
 */
public class K_PickOneWordPerConcept {
    /**
     * Pick one word for one abstract concept and stick with it. For instance,
     * it’s confusing to have fetch, retrieve, and get as equivalent methods of
     * different classes. How do you remember which method name goes with which
     * class? Sadly, you often have to remember which company, group, or
     * individual wrote the library or class in order to remember which term was
     * used. Otherwise, you spend an awful lot of time browsing through headers
     * and previous code samples.
     *
     * Modern editing environments like Eclipse and IntelliJ-provide
     * context-sensitive clues, such as the list of methods you can call on a
     * given object. But note that the list doesn’t usually give you the
     * comments you wrote around your function names and parameter lists. You
     * are lucky if it gives the parameter names from function declarations. The
     * function names have to stand alone, and they have to be consistent in
     * order for you to pick the correct method without any additional
     * exploration.
     *
     * Likewise, it’s confusing to have a controller and a manager and a driver
     * in the same code base. What is the essential difference between a
     * DeviceManager and a Protocol- Controller? Why are both not controller s
     * or both not managers? Are they both Drivers really? The name leads you to
     * expect two objects that have very different type as well as having
     * different classes.
     *
     * A consistent lexicon is a great boon to the programmers who must use your
     * code.
     */
}
