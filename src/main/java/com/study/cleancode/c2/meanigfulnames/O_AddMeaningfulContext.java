package com.study.cleancode.c2.meanigfulnames;


/**
 *
 * @author ctagler
 */
public class O_AddMeaningfulContext {

    /**
     * There are a few names which are meaningful in and of themselves, most are
     * not. Instead, you need to place names in context for your reader by
     * enclosing them in well-named classes, functions, or namespaces. When all
     * else fails, then prefixing the name may be necessary as a last resort.
     *
     * Imagine that you have variables named firstName, lastName, street,
     * houseNumber, city, state, and zipcode. Taken together it’s pretty clear
     * that they form an address. But what if you just saw the state variable
     * being used alone in a method? Would you automatically infer that it was
     * part of an address?
     *
     * You can add context by using prefixes: addrFirstName , addrLastName ,
     * addrState , and so on. At least readers will understand that these
     * variables are part of a larger structure. Of course, a better solution is
     * to create a class named Address. Then, even the compiler knows that the
     * variables belong to a bigger concept.
     *
     * Consider the method in Listing 2-1. Do the variables need a more
     * meaningful context? The function name provides only part of the context;
     * the algorithm provides the rest. Once you read through the function, you
     * see that the three variables, number, verb, and pluralModifier, are part
     * of the “guess statistics” message. Unfortunately, the context must be
     * inferred. When you first look at the method, the meanings of the
     * variables are opaque.
     *
     * Listing 2-1 Variables with unclear context.
     */
    private void printGuessStatistics(char candidate, int count) {
        String number;
        String verb;
        String pluralModifier;
        if (count == 0) {
            number = "no";
            verb = "are";
            pluralModifier = "s";
        } else if (count == 1) {
            number = "1";
            verb = "is";
            pluralModifier = "";
        } else {
            number = Integer.toString(count);
            verb = "are";
            pluralModifier = "s";
        }
        String guessMessage = String.format("There %s %s %s%s", verb, number, candidate, pluralModifier);
        print(guessMessage);
    }

    private void print(String guessMessage) {
        System.out.println(guessMessage);
    }

    /**
     * The function is a bit too long and the variables are used throughout. To
     * split the function into smaller pieces we need to create a
     * GuessStatisticsMessage class and make the three variables fields of this
     * class. This provides a clear context for the three variables. They are
     * definitively part of the GuessStatisticsMessage . The improvement of
     * context also allows the algorithm to be made much cleaner by breaking it
     * into many smaller functions. (See Listing 2-2.)
     *
     * Listing 2-2 Variables have a context.
     */
    public class GuessStatisticsMessage {

        private String number;
        private String verb;
        private String pluralModifier;

        public String make(char candidate, int count) {
            createPluralDependentMessageParts(count);
            return String.format(
                    "There %s %s %s%s",
                    verb, number, candidate, pluralModifier);
        }

        private void createPluralDependentMessageParts(int count) {
            if (count == 0) {
                thereAreNoLetters();
            } else if (count == 1) {
                thereIsOneLetter();
            } else {
                thereAreManyLetters(count);
            }
        }

        private void thereAreManyLetters(int count) {
            number = Integer.toString(count);
            verb = "are";
            pluralModifier = "s";
        }

        private void thereIsOneLetter() {
            number = "1";
            verb = "is";
            pluralModifier = "";
        }

        private void thereAreNoLetters() {
            number = "no";
            verb = "are";
            pluralModifier = "s";
        }
    }

}
