package com.study.cleancode.c2.meanigfulnames;

/**
 *
 * @author ctagler
 */
public class E_UseSearchableNames {

    private int[] t, taskEstimate;
    private int s;

    public E_UseSearchableNames() {
        /**
         * Single-letter names and numeric constants have a particular problem
         * in that they are not easy to locate across a body of text.
         *
         * One might easily grep for MAX_CLASSES_PER_STUDENT , but the number 7
         * could be more troublesome. Searches may turn up the digit as part of
         * file names, other constant definitions, and in various expressions
         * where the value is used with different intent. It is even worse when
         * a constant is a long number and someone might have transposed digits,
         * thereby creating a bug while simultaneously evading the programmer’s
         * search.
         *
         * Likewise, the name e is a poor choice for any variable for which a
         * programmer might need to search. It is the most common letter in the
         * English language and likely to show up in every passage of text in
         * every program. In this regard, longer names trump shorter names, and
         * any searchable name trumps a constant in code.
         *
         * My personal preference is that single-letter names can ONLY be used
         * as local variables inside short methods. The length of a name should
         * correspond to the size of its scope. If a variable or constant might
         * be seen or used in multiple places in a body of code, it is
         * imperative to give it a search-friendly name. Once again compare
         */
        for (int j = 0; j < 34; j++) {
            s += (t[j] * 4) / 5;
        }

        /**
         * to
         */
        int realDaysPerIdealDay = 4;
        int WORK_DAYS_PER_WEEK = 5;
        int NUMBER_OF_TASKS = 34;
        int sum = 0;
        for (int j = 0; j < NUMBER_OF_TASKS; j++) {
            int realTaskDays = taskEstimate[j] * realDaysPerIdealDay;
            int realTaskWeeks = (realTaskDays / WORK_DAYS_PER_WEEK);
            sum += realTaskWeeks;
        }

        /**
         * Note that sum, above, is not a particularly useful name but at least
         * is searchable. The intentionally named code makes for a longer
         * function, but consider how much easier it will be to find
         * WORK_DAYS_PER_WEEK than to find all the places where 5 was used and
         * filter the list down to just the instances with the intended meaning.
         */
    }
}
