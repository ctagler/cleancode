package com.study.cleancode.c2.meanigfulnames;

import com.study.cleancode.c2.meanigfulnames.classes.Complex;
import com.study.cleancode.c2.meanigfulnames.classes.customer;
import com.study.cleancode.c2.meanigfulnames.classes.employee;
import com.study.cleancode.c2.meanigfulnames.classes.paycheck;

/**
 *
 * @author ctagler
 */
public class I_MethodNames {

    public void main() {
        /**
         * Methods should have verb or verb phrase names like postPayment,
         * deletePage, or save. Accessors, mutators, and predicates should be
         * named for their value and prefixed with get, set, and is according to
         * the javabean standard.
         */
        String name = employee.getName();

        customer.setName("mike");

        if (paycheck.isPosted()) {
            // doing something
        }

        /**
         * When constructors are overloaded, use static factory methods with
         * names that describe the arguments. For example:
         */
        Complex fulcrumPoint = Complex.FromRealNumber(23.0);

        /**
         * is generally better than
         */
        Complex fulcrumPoint2 = new Complex(23.0);

        /**
         * Consider enforcing their use by making the corresponding constructors
         * private.
         */
    }
}
