package com.study.cleancode.c2.meanigfulnames;

/**
 *
 * @author ctagler
 */
public class G_AvoidMentalMapping {

    /**
     * Readers shouldn’t have to mentally translate your names into other names
     * they already know. This problem generally arises from a choice to use
     * neither problem domain terms nor solution domain terms.
     *
     * This is a problem with single-letter variable names. Certainly a loop
     * counter may be named i or j or k (though never l !) if its scope is very
     * small and no other names can conflict with it. This is because those
     * single-letter names for loop counters are traditional. However, in most
     * other contexts a single-letter name is a poor choice; it’s just a place
     * holder that the reader must mentally map to the actual concept. There can
     * be no worse reason for using the name c than because a and b were already
     * taken.
     *
     * In general programmers are pretty smart people. Smart people sometimes
     * like to show off their smarts by demonstrating their mental juggling
     * abilities. After all, if you can reliably remember that r is the
     * lower-cased version of the url with the host and scheme removed, then you
     * must clearly be very smart.
     *
     * One difference between a smart programmer and a professional programmer
     * is that the professional understands that clarity is king. Professionals
     * use their powers for good and write code that others can understand.
     */
}
