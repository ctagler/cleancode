package com.study.cleancode.c3.funtions;

import com.study.cleancode.c3.funtions.classes.WikiPagePath;
import com.study.cleancode.c3.funtions.classes.PathParser;
import com.study.cleancode.c3.funtions.classes.SuiteResponder;
import com.study.cleancode.c3.funtions.classes.PageCrawlerImpl;
import com.study.cleancode.c3.funtions.classes.WikiPage;
import com.study.cleancode.c3.funtions.classes.PageData;

/**
 *
 * @author ctagler
 *
 * In the early days of programming we composed our systems of routines and
 * subroutines. Then, in the era of Fortran and PL/1 we composed our systems of
 * programs, subprograms, and functions. Nowadays only the function survives
 * from those early days. Functions are the first line of organization in any
 * program. Writing them well is the topic of this chapter.
 *
 * Consider the code in Listing 3-1. It’s hard to find a long function in
 * FitNesse, (An open-source testing tool. www.fitnese.org) but after a bit of
 * searching I came across this one. Not only is it long, but it’s got
 * duplicated code, lots of odd strings, and many strange and inobvious data
 * types and APIs. See how much sense you can make of it in the next three
 * minutes.
 */
@SuppressWarnings("StringBufferMayBeStringBuilder")
public class HtmlUtil {

    /**
     * Listing 3-1 HtmlUtil.java (FitNesse 20070619)
     *
     * @param pageData
     * @param includeSuiteSetup
     * @return
     * @throws java.lang.Exception
     */
    public static String testableHtml(PageData pageData, boolean includeSuiteSetup) throws Exception {
        WikiPage wikiPage = pageData.getWikiPage();
        StringBuffer buffer = new StringBuffer();
        if (pageData.hasAttribute("Test")) {
            if (includeSuiteSetup) {
                WikiPage suiteSetup
                        = PageCrawlerImpl.getInheritedPage(
                                SuiteResponder.SUITE_SETUP_NAME, wikiPage
                        );
                if (suiteSetup != null) {
                    WikiPagePath pagePath
                            = suiteSetup.getPageCrawler().getFullPath(suiteSetup);
                    String pagePathName = PathParser.render(pagePath);
                    buffer.append("!include -setup .")
                            .append(pagePathName)
                            .append("\n");
                }
            }
            WikiPage setup
                    = PageCrawlerImpl.getInheritedPage("SetUp", wikiPage);
            if (setup != null) {
                WikiPagePath setupPath
                        = wikiPage.getPageCrawler().getFullPath(setup);
                String setupPathName = PathParser.render(setupPath);
                buffer.append("!include -setup .")
                        .append(setupPathName)
                        .append("\n");
            }
        }
        buffer.append(pageData.getContent());
        if (pageData.hasAttribute("Test")) {
            WikiPage teardown
                    = PageCrawlerImpl.getInheritedPage("TearDown", wikiPage);
            if (teardown != null) {
                WikiPagePath tearDownPath
                        = wikiPage.getPageCrawler().getFullPath(teardown);
                String tearDownPathName = PathParser.render(tearDownPath);
                buffer.append("\n")
                        .append("!include -teardown .")
                        .append(tearDownPathName)
                        .append("\n");
            }
            if (includeSuiteSetup) {
                WikiPage suiteTeardown
                        = PageCrawlerImpl.getInheritedPage(
                                SuiteResponder.SUITE_TEARDOWN_NAME,
                                wikiPage
                        );
                if (suiteTeardown != null) {
                    WikiPagePath pagePath
                            = suiteTeardown.getPageCrawler().getFullPath(suiteTeardown);
                    String pagePathName = PathParser.render(pagePath);
                    buffer.append("!include -teardown .")
                            .append(pagePathName)
                            .append("\n");
                }
            }
        }
        pageData.setContent(buffer.toString());
        return pageData.getHtml();
    }

    /**
     * Do you understand the function after three minutes of study? Probably
     * not. There’s too much going on in there at too many different levels of
     * abstraction. There are strange strings and odd function calls mixed in
     * with doubly nested if statements controlled by flags.
     *
     * However, with just a few simple method extractions, some renaming, and a
     * little restructuring, I was able to capture the intent of the function in
     * the nine lines of Listing 3-2. See whether you can understand that in the
     * next 3 minutes.
     *
     * Listing 3-2 HtmlUtil.java (refactored)
     *
     * @param pageData
     * @param isSuite
     * @throws java.lang.Exception
     * @return
     */
    public static String renderPageWithSetupsAndTeardowns(PageData pageData, boolean isSuite) throws Exception {
        boolean isTestPage = pageData.hasAttribute("Test");
        if (isTestPage) {
            WikiPage testPage = pageData.getWikiPage();
            StringBuffer newPageContent = new StringBuffer();
            includeSetupPages(testPage, newPageContent, isSuite);
            newPageContent.append(pageData.getContent());
            includeTeardownPages(testPage, newPageContent, isSuite);
            pageData.setContent(newPageContent.toString());
        }
        return pageData.getHtml();
    }

    /**
     * Unless you are a student of FitNesse, you probably don’t understand all
     * the details. Still, you probably understand that this function performs
     * the inclusion of some setup and teardown pages into a test page and then
     * renders that page into HTML. If you are familiar with JUnit (An
     * open-source unit-testing tool for Java. www.junit.org), you probably
     * realize that this function belongs to some kind of Web-based testing
     * framework. And, of course, that is correct. Divining that information
     * from Listing 3-2 is pretty easy, but it’s pretty well obscured by Listing
     * 3-1.
     *
     * So what is it that makes a function like Listing 3-2 easy to read and
     * understand? How can we make a function communicate its intent? What
     * attributes can we give our functions that will allow a casual reader to
     * intuit the kind of program they live inside?
     */
    private static void includeSetupPages(WikiPage testPage, StringBuffer newPageContent, boolean suite) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void includeTeardownPages(WikiPage testPage, StringBuffer newPageContent, boolean suite) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * *************************************************************************
     * Small!
     *
     * The first rule of functions is that they should be small. The second rule
     * of functions is that they should be smaller than that. This is not an
     * assertion that I can justify. I can’t provide any references to research
     * that shows that very small functions are better. What I can tell you is
     * that for nearly four decades I have written functions of all different
     * sizes. I’ve written several nasty 3,000-line abominations. I’ve written
     * scads of functions in the 100 to 300 line range. And I’ve written
     * functions that were 20 to 30 lines long. What this experience has taught
     * me, through long trial and error, is that functions should be very Small.
     *
     * In the eighties we used to say that a function should be no bigger than a
     * screen-full. Of course we said that at a time when VT100 screens were 24
     * lines by 80 columns, and our editors used 4 lines for administrative
     * purposes. Nowadays with a cranked-down font and a nice big monitor, you
     * can fit 150 characters on a line and a 100 lines or more on a screen.
     * Lines should not be 150 characters long. Functions should not be 100
     * lines long. Functions should hardly ever be 20 lines long.
     *
     * How short should a function be? In 1999 I went to visit Kent Beck at his
     * home in Oregon. We sat down and did some programming together. At one
     * point he showed me a cute little Java/Swing program that he called
     * Sparkle. It produced a visual effect on the screen very similar to the
     * magic wand of the fairy godmother in the movie Cinderella. As you moved
     * the mouse, the sparkles would drip from the cursor with a satisfying
     * scintillation, falling to the bottom of the window through a simulated
     * gravitational field. When Kent showed me the code, I was struck by how
     * small all the functions were. I was used to functions in Swing programs
     * that took up miles of vertical space. Every function in this program was
     * just two, or three, or four lines long. Each was transparently obvious.
     * Each told a story. And each led you to the next in a compelling order.
     * That’s how short your functions should be! (I asked Kent whether he still
     * had a copy, but he was unable to find one. I searched all my old
     * computers too, but to no avail. All that is left now is my memory of that
     * program.)
     *
     * How short should your functions be? They should usually be shorter than
     * Listing 3-2! Indeed, Listing 3-2 should really be shortened to Listing
     * 3-3.
     *
     * Listing 3-3 HtmlUtil.java (re-refactored)
     *
     * @param pageData
     * @param isSuite
     * @return
     * @throws java.lang.Exception
     */
    public static String renderPageWithSetupsAndTeardownsR(PageData pageData, boolean isSuite) throws Exception {
        if (isTestPage(pageData)) {
            includeSetupAndTeardownPages(pageData, isSuite);
        }
        return pageData.getHtml();
    }

    private static boolean isTestPage(PageData pageData) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void includeSetupAndTeardownPages(PageData pageData, boolean suite) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Blocks and Indenting
     *
     * This implies that the blocks within if statements, else statements, while
     * statements, and so on should be one line long. Probably that line should
     * be a function call. Not only does this keep the enclosing function small,
     * but it also adds documentary value because the function called within the
     * block can have a nicely descriptive name.
     *
     * This also implies that functions should not be large enough to hold
     * nested structures. Therefore, the indent level of a function should not
     * be greater than one or two. This, of course, makes the functions easier
     * to read and understand.
     */
    /**
     * *************************************************************************
     * Do One Thing
     *
     * It should be very clear that Listing 3-1 is doing lots more than one
     * thing. It’s creating buffers, fetching pages, searching for inherited
     * pages, rendering paths, appending arcane strings, and generating HTML,
     * among other things. Listing 3-1 is very busy doing lots of different
     * things. On the other hand, Listing 3-3 is doing one simple thing. It’s
     * including setups and teardowns into test pages.
     *
     * The following advice has appeared in one form or another for 30 years or
     * more.
     *
     * FUNCTIONS SHOULD DO ONE THING. THEY SHOULD DO IT WELL. THEY SHOULD DO IT
     * ONLY.
     *
     * The problem with this statement is that it is hard to know what “one
     * thing” is. Does Listing 3-3 do one thing? It’s easy to make the case that
     * it’s doing three things:
     *
     * 1. Determining whether the page is a test page.
     *
     * 2. If so, including setups and teardowns.
     *
     * 3. Rendering the page in HTML.
     *
     * So which is it? Is the function doing one thing or three things? Notice
     * that the three steps of the function are one level of abstraction below
     * the stated name of the function. We can describe the function by
     * describing it as a brief TO (The LOGO language used the keyword “TO” in
     * the same way that Ruby and Python use “def.” So every function began with
     * the word “TO.” This had an interesting effect on the way functions were
     * designed.) paragraph:
     *
     * TO RenderPageWithSetupsAndTeardowns, we check to see whether the page is
     * a test page and if so, we include the setups and teardowns. In either
     * case we render the page in HTML.
     *
     * If a function does only those steps that are one level below the stated
     * name of the function, then the function is doing one thing. After all,
     * the reason we write functions is to decompose a larger concept (in other
     * words, the name of the function) into a set of steps at the next level of
     * abstraction.
     *
     * It should be very clear that Listing 3-1 contains steps at many different
     * levels of abstraction. So it is clearly doing more than one thing. Even
     * Listing 3-2 has two levels of abstraction, as proved by our ability to
     * shrink it down. But it would be very hard to meaningfully shrink Listing
     * 3-3. We could extract the if statement into a function named
     * includeSetupsAndTeardownsIfTestPage, but that simply restates the code
     * without changing the level of abstraction.
     *
     * So, another way to know that a function is doing more than “one thing” is
     * if you can extract another function from it with a name that is not
     * merely a restatement of its implementation [G34].
     *
     * Sections within Functions
     *
     * Look at Listing 4-7 on page 71. Notice that the generatePrimes function
     * is divided into sections such as declarations, initializations, and
     * sieve. This is an obvious symptom of doing more than one thing. Functions
     * that do one thing cannot be reasonably divided into sections.
     */
    /**
     * *************************************************************************
     * One Level of Abstraction per Function
     *
     * In order to make sure our functions are doing “one thing,” we need to
     * make sure that the statements within our function are all at the same
     * level of abstraction. It is easy to see how Listing 3-1 violates this
     * rule. There are concepts in there that are at a very high level of
     * abstraction, such as
     *
     * getHtml();
     *
     * others that are at an intermediate level of abstraction, such as:
     *
     * String pagePathName = PathParser.render(pagePath) ;
     *
     * and still others that are remarkably low level, such as:
     *
     * .append("\n").
     *
     * Mixing levels of abstraction within a function is always confusing.
     * Readers may not be able to tell whether a particular expression is an
     * essential concept or a detail. Worse, like broken windows, once details
     * are mixed with essential concepts, more and more details tend to accrete
     * within the function.
     *
     * Reading Code from Top to Bottom: The Stepdown Rule
     *
     * We want the code to read like a top-down narrative ([KP78], p. 37.) We
     * want every function to be followed by those at the next level of
     * abstraction so that we can read the program, descending one level of
     * abstraction at a time as we read down the list of functions. I call this
     * The Stepdown Rule.
     *
     * To say this differently, we want to be able to read the program as though
     * it were a set of TO paragraphs, each of which is describing the current
     * level of abstraction and referencing subsequent TO paragraphs at the next
     * level down.
     *
     * To include the setups and teardowns, we include setups, then we include
     * the test page content, and then we include the teardowns.
     *
     * To include the setups, we include the suite setup if this is a suite,
     * then we include the regular setup.
     *
     * To include the suite setup, we search the parent hierarchy for the
     * “SuiteSetUp” page and add an include statement with the path of that
     * page.
     *
     * To search the parent. . .
     *
     * It turns out to be very difficult for programmers to learn to follow this
     * rule and write functions that stay at a single level of abstraction. But
     * learning this trick is also very important. It is the key to keeping
     * functions short and making sure they do “one thing.” Making the code read
     * like a top-down set of TO paragraphs is an effective technique for
     * keeping the abstraction level consistent.
     *
     * Take a look at Listing 3-7 at the end of this chapter. It shows the whole
     * testableHtml function refactored according to the principles described
     * here. Notice how each function introduces the next, and each function
     * remains at a consistent level of abstraction.
     */
}
