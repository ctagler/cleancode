package com.study.cleancode.c3.funtions;

import com.study.cleancode.c3.funtions.classes.EmployeeRecord;
import com.study.cleancode.c3.funtions.classes.InvalidEmployeeType;
import com.study.cleancode.c3.funtions.classes.Money;

/**
 *
 * @author ctagler
 */
public class Payroll {

    final int COMMISSIONED = 1;
    final int HOURLY = 2;
    final int SALARIED = 3;
    final int NONE = 4;

    /**
     * *************************************************************************
     * Switch Statements
     *
     * It’s hard to make a small switch statement (And, of course, I include
     * if/else chains in this.). Even a switch statement with only two cases is
     * larger than I’d like a single block or function to be. It’s also hard to
     * make a switch statement that does one thing. By their nature, switch
     * statements always do N things. Unfortunately we can’t always avoid switch
     * statements, but we can make sure that each switch statement is buried in
     * a low-level class and is never repeated. We do this, of course, with
     * polymorphism.
     *
     * Consider Listing 3-4. It shows just one of the operations that might
     * depend on the type of employee.
     *
     * Listing 3-4 Payroll.java
     *
     * @param e
     * @return
     * @throws com.study.cleancode.c3.funtions.classes.InvalidEmployeeType
     */
    public Money calculatePay(com.study.cleancode.c3.funtions.classes.Employee e) throws InvalidEmployeeType {
        switch (e.type) {
            case COMMISSIONED:
                return calculateCommissionedPay(e);
            case HOURLY:
                return calculateHourlyPay(e);
            case SALARIED:
                return calculateSalariedPay(e);
            default:
                throw new InvalidEmployeeType(e.type);
        }
    }

    private Money calculateCommissionedPay(com.study.cleancode.c3.funtions.classes.Employee e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Money calculateHourlyPay(com.study.cleancode.c3.funtions.classes.Employee e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Money calculateSalariedPay(com.study.cleancode.c3.funtions.classes.Employee e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * There are several problems with this function. First, it’s large, and
     * when new employee types are added, it will grow. Second, it very clearly
     * does more than one thing. Third, it violates the Single Responsibility
     * Principle (SRP)
     *
     * (a. http://en.wikipedia.org/wiki/Single_responsibility_principle
     *
     * b. http://www.objectmentor.com/resources/articles/srp.pdf)
     *
     * because there is more than one reason for it to change. Fourth, it
     * violates the Open Closed Principle (OCP)
     *
     * (a. http://en.wikipedia.org/wiki/Open/closed_principle
     *
     * b. http://www.objectmentor.com/resources/articles/ocp.pdf)
     *
     * because it must change whenever new types are added. But possibly the
     * worst problem with this function is that there are an unlimited number of
     * other functions that will have the same structure. For example we could
     * have
     *
     * isPayday(Employee e, Date date)
     *
     * , or
     *
     * deliverPay(Employee e, Money pay)
     *
     * , or a host of others. All of which would have the same deleterious
     * structure.
     *
     * The solution to this problem (see Listing 3-5) is to bury the switch
     * statement in the basement of an ABSTRACT FACTORY [GOF], and never let
     * anyone see it. The factory will use the switch statement to create
     * appropriate instances of the derivatives of Employee, and the various
     * functions, such as calculatePay, isPayday, and deliverPay, will be
     * dispatched polymorphically through the Employee interface.
     *
     * My general rule for switch statements is that they can be tolerated if
     * they appear only once, are used to create polymorphic objects, and are
     * hidden behind an inheritance.
     *
     * Listing 3-5 Employee and Factory
     */
    public abstract class Employee {

        public abstract boolean isPayday();

        public abstract Money calculatePay();

        public abstract void deliverPay(Money pay);
    }

    public interface EmployeeFactory {

        public Employee makeEmployee(EmployeeRecord r) throws InvalidEmployeeType;
    }

    public class EmployeeFactoryImpl implements EmployeeFactory {

        @Override
        public Employee makeEmployee(EmployeeRecord r) throws InvalidEmployeeType {
            switch (r.type) {
                case COMMISSIONED:
                    return new CommissionedEmployee(r);
                case HOURLY:
                    return new HourlyEmployee(r);
                case SALARIED:
                    return new SalariedEmploye(r);
                default:
                    throw new InvalidEmployeeType(r.type);
            }
        }
    }

    /**
     */
    /**
     */
    /**
     */
    /**
     */
    /**
     */
}
